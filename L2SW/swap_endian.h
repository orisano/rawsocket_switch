#ifndef SWAPENDIAN_H
#define SWAPENDIAN_H

#include <stdint.h>

uint32_t swap_32(uint32_t n);
uint16_t swap_16(uint16_t n);

#endif
